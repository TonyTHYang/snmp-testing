from pysnmp.entity import engine, config
from pysnmp.carrier.asyncore.dgram import udp
from pysnmp.entity.rfc3413 import ntfrcv
from pysnmp.proto.api import v2c

TrapAgentAddress = '10.123.15.163'
Port = 162
username = 'tester'
# Create SNMP engine with autogenernated engineID and pre-bound
# to socket transport dispatcher
snmpEngine = engine.SnmpEngine()

print('Agent is Receiving SNMP Trap on %s , Port: %s' % (TrapAgentAddress,str(Port)))
print('--------------------------------------------------------------------------')
# Transport setup

# UDP over IPv4
config.addTransport(
    snmpEngine,
    udp.domainName,
    udp.UdpTransport().openServerMode((TrapAgentAddress, Port))
)

# SNMPv3/USM setup

# user: usr-md5-des, auth: MD5, priv DES, securityEngineId: 8000000001020304
# this USM entry is configured for TRAP receiving purposes
config.addV3User(
    snmpEngine, username,
    config.usmHMACMD5AuthProtocol,'admin123',
    config.usmDESPrivProtocol,'admin123',
    securityEngineId=v2c.OctetString(hexValue='80001f8804307838303030323166333033303039306538306630613033')
    )

#    securityEngineId=v2c.OctetString(hexValue='8000000001020304'

# Callback function for receiving notifications
# noinspection PyUnusedLocal,PyUnusedLocal,PyUnusedLocal
def cbFun(snmpEngine, stateReference, contextEngineId, contextName,
          varBinds, cbCtx):
    print("Received new Trap message")
    for name, val in varBinds:
        print('%s = %s' % (name.prettyPrint(), val.prettyPrint()))


# Register SNMP Application at the SNMP engine
ntfrcv.NotificationReceiver(snmpEngine, cbFun)

snmpEngine.transportDispatcher.jobStarted(1)  # this job would never finish

# Run I/O dispatcher which would receive queries and send confirmations
try:
    print('Trap Receiver started on port %s. Press Ctrl-c to quit.' % (Port))
    snmpEngine.transportDispatcher.runDispatcher()

finally:
    print('Trap Receiver closed.')
    snmpEngine.transportDispatcher.closeDispatcher()
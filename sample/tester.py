from snmp.snmp import *

if __name__ == '__main__':

    target_ip:str = '10.123.17.190'
    trap_ip:str = '10.123.15.163'
    test_oid:str = '1.3.6.1.2.1.1.5'
    mib2_sys_oid:str ='.1.3.6.1.2.1.1'
    engineid_oid:str ='1.3.6.1.6.3.10.2.1'

    engineId:str = '80001f8804307838303030323166333033303039306538306630613033'

    kwargs_snmpwalk_v3 = {
        'user_name':'aramco','oid':engineid_oid,
        'auth_key':'admin123','priv_key':'admin123'
        }

    kwargs_snmp_trap_v3 = {
        'user_name':'tester',
        'auth_proto':Public_Auth.MD5_Auth.value,
        'priv_proto':Private_Auth.DES_Priv.value,
        'auth_key':'admin123',
        'priv_key':'admin123',
        'securityEngineId':engineId
    }

    #SNMP(Agent_Model.V3,target_ip,161,**kwargs_snmpwalk_v3).snmp_walk()
    #SNMP(Agent_Model.V3,trap_ip,162,**kwargs_snmp_trap_v3).snmp_trap_receiver()

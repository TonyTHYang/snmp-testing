#python snmp trap receiver
from pysnmp.entity import engine, config
from pysnmp.carrier.asyncore.dgram import udp
from pysnmp.entity.rfc3413 import ntfrcv
from utils.logger import logger

logger = logger('Trap-Receiver','Trap-Receiver')

def Run_SNMP_Trap_Receiver(TrapAgentAddress:str,Port:int):
    '''
    TrapAgentAddress: Trap Receiver address
    Port: trap Receiver port
    '''
    snmpEngine = engine.SnmpEngine()

    logger.log_info('Agent is Receiving SNMP Trap on %s , Port: %s' % (TrapAgentAddress,str(Port)))
    logger.log_info('--------------------------------------------------------------------------')

    config.addTransport(
        snmpEngine,
        udp.domainName,
        udp.UdpTransport().openServerMode((TrapAgentAddress, Port))
    )

    # SNMPv1/2c setup
    config.addV1System(snmpEngine, 'public', 'public')

    # Callback function for receiving notifications
    def cbFun(snmpEngine, stateReference, contextEngineId, contextName,varBinds, cbCtx):
        logger.log_info("Received new Trap message")
        for name, val in varBinds:        
            logger.log_info('%s = %s' % (name.prettyPrint(), val.prettyPrint()))

    # Register SNMP Application at the SNMP engine
    ntfrcv.NotificationReceiver(snmpEngine, cbFun)

    snmpEngine.transportDispatcher.jobStarted(1)  

    try:
        logger.log_info('Trap Receiver started on port %s. Press Ctrl-c to quit.' % (Port))
        snmpEngine.transportDispatcher.runDispatcher()
    except:
        logger.log_info('Trap Receiver closed.')
        snmpEngine.transportDispatcher.closeDispatcher()
        raise

if __name__ == "__main__":
    Run_SNMP_Trap_Receiver('10.123.15.163',162)
import time
import logging
import distutils.dir_util

class logger(object):

    def __init__(self,log_path:str,log_file:str):

        self.log_path = '%s/%s' % ('log',log_path)
        self.log_file = log_file

        distutils.dir_util.mkpath(self.log_path)
        log:str = '%s/%s_%s.log' %(self.log_path,self.log_file,time.strftime('%Y%m%d_%H%M%S'))
        logging.basicConfig(filename=log,level=logging.INFO)


    def log_info(self,data:str):
        
        self.data = '%s - %s' %(time.strftime("%H:%M:%S"),data)
        print(self.data)
        logging.info(self.data)

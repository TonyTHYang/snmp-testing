from snmp.snmp import *
from tests.init_cofig import *

setups:classmethod = init_test('/home/tony/snmp-test/config/SNMP_v3')

def generate_inform(info:str):    
    snmp = SNMP(init_Entry(),snmp_setup=setups)
    snmp.inform_generator(info)

if __name__ == '__main__':
    generate_inform('1.3.6.1.6.3.1.1.5.2')
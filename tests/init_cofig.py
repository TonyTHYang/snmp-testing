import configparser
from pysnmp.hlapi import *
from snmp.snmp import Entry,SNMP_v12c,SNMP_v3,Public_Auth,Private_Auth,Agent_Model

dut_config = configparser.ConfigParser()

def init_Entry()->Entry:
    agent_ip:str = dut_config['Entry']['agent_ip']
    agent_port:int = dut_config['Entry'].getint('agent_port')
    host_ip:str = dut_config['Entry']['host_ip']
    host_port:int = dut_config['Entry'].getint('host_port')
    
    return Entry(agent_ip,agent_port,host_ip,host_port)


def init_snmp_v12c()->SNMP_v12c:
    community_name:str = dut_config['SNMP_v12c']['community_name']
    community_index:str = dut_config['SNMP_v12c']['community_index']
    auth_data:object = CommunityData(community_name,community_index)

    return SNMP_v12c(Agent_Model.V1v2c,community_name,community_index,auth_data)


def init_snmp_v3()->SNMP_v3:
    host_name:str = dut_config['SNMP_v3']['host_name']
    user_name:str = dut_config['SNMP_v3']['user_name']
    public_opt:str = dut_config['SNMP_v3']['public_protocol']
    private_opt:str = dut_config['SNMP_v3']['private_protocol']
    public_key:str = dut_config['SNMP_v3']['public_key']
    private_key:str = dut_config['SNMP_v3']['private_key']
    
    # Public protocol setting
    if(public_opt=='MD5'):
        public_protocol = Public_Auth.MD5.value
    elif(public_opt=='SHA'):
        public_protocol = Public_Auth.SHA.value
    elif(public_opt=='NO'):
        public_protocol = Public_Auth.NO.value
    else:
        public_protocol = None

    # Private protocol setting
    if(private_opt=='DES'):
        private_protocol = Private_Auth.DES.value
    elif(private_opt=='AES128'):
        private_protocol = Private_Auth.AES128.value
    elif(private_opt=='AES192'):
        private_protocol = Private_Auth.AES192.value
    elif(private_opt=='AES256'):
        private_protocol = Private_Auth.AES256.value
    elif(private_opt=='NO'):
        private_protocol = Private_Auth.NO.value
    else:
        private_protocol =None

    auth_data:object = UsmUserData(host_name,public_key,private_key)
    snmp_v3 = SNMP_v3(
        Agent_Model.V3,host_name,user_name,
        public_protocol,private_protocol,public_key,private_key,auth_data)

    return snmp_v3


def init_test(config_path:str)->classmethod:

    dut_config.read(f"{config_path}/dut.conf")
    test_mode:str = dut_config['Test_Mode']['mode']

    if (test_mode=='v12c'): 
        setup = init_snmp_v12c()
    elif(test_mode=='v3'):
        setup = init_snmp_v3()

    return setup
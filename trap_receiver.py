from collections import deque

from snmp.snmp import *
from tests.init_cofig import *

setups:classmethod = init_test('/home/tony/snmp-test/config/SNMP_v3')

def snmp_v3_trap_receiver():    
    snmp = SNMP(init_Entry(),snmp_setup=setups)
    queue:deque = snmp.trap_receiver()

    print('\n\ndeque received:\n%s'%(queue))

if __name__ == '__main__':
    snmp_v3_trap_receiver()
    

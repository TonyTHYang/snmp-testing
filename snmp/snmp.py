from enum import Enum
from enum import IntEnum
from collections import deque
from dataclasses import dataclass 

from pysnmp.hlapi import *
from pysnmp.entity import engine, config
from pysnmp.entity.rfc3413 import ntfrcv
from pysnmp.carrier.asyncore.dgram import udp
from pysnmp.proto.api import v2c
from pysnmp.smi import builder, view, compiler, rfc1902

from utils.logger import logger


class Agent_Model(IntEnum):
    V1v2c = 0
    V3 = 1


class Public_Auth(Enum):
    NO = config.usmNoAuthProtocol
    MD5 = config.usmHMACMD5AuthProtocol
    SHA = config.usmHMACSHAAuthProtocol
    

class Private_Auth(Enum):
    NO = config.usmNoPrivProtocol
    DES = config.usmDESPrivProtocol
    AES128 = config.usmAesCfb128Protocol
    AES192 = config.usmAesCfb192Protocol
    AES256 = config.usmAesCfb256Protocol


@dataclass
class Entry:
    agent_ip:str 
    agent_port:int
    host_ip:str
    host_port:int


@dataclass
class SNMP_v12c:
    ag_mode:Agent_Model
    community_name:str = None
    community_index:str = None
    auth_data:object =None


@dataclass
class SNMP_v3:
    ag_mode:Agent_Model 
    host_name:str = None
    user_name:str = None
    public_Auth:Public_Auth = None
    private_Auth:Private_Auth  = None
    public_key:str = None
    private_key:str = None
    auth_data:object =None


class SNMP(object):
    """SNMP (available for v1/v2c,v3)
    
    Parameters:
    ----------
        *entry: :py:class:`Entry`
                Host/Agent's IP address and UDP port
        *snmp_setup: :py:class:`classmethod`
                Snmp v1/2c or v3 setup

    Function:
    ----------
        *get_cmd:           
                get a single OID information, 
                return: :py:class:`str`
        *snmp_walk:         
                Parsing MIB OID information,
                return: :py:class:`list`
        *trap_Receiver:     
                Receive Trap information,
                return: :py:class:`deque`

    Method:
    ----------
        *inform_generator:  Create an inform package
        *load_mib:          Load mib file from internet

    Property:
    ----------
        *Get engine id:     request SNMP engine ID
        *Set engine id:     Set SNMP engine ID
    """

    def __init__(self,entry:Entry,snmp_setup:classmethod):

        self._engine = engine.SnmpEngine()
        self._entry = entry
        self._setup = snmp_setup
        self.set_engine_id = self.get_engine_id
        self.load_mib()
        

    @property
    def get_engine_id(self)->str:
        info:str = str(self.get_cmd('1.3.6.1.6.3.10.2.1.1.0'))
        return info[info.find('0x')+2:] 


    @get_engine_id.setter
    def set_engine_id(self,engine_id):
        self._engine_id = engine_id


    def get_cmd(self,oid)->str:
        '''
        Get a single OID information
        '''
        iterator = getCmd(
            self._engine,
            self._setup.auth_data,
            UdpTransportTarget((self._entry.agent_ip,self._entry.agent_port)),
            ContextData(),
            ObjectType(ObjectIdentity(oid))
        )
        errorIndication, errorStatus, errorIndex, varBinds = next(iterator)

        if errorIndication:
            print(errorIndication)
        elif errorStatus:
            print('%s at %s' % (errorStatus.prettyPrint(),
                                errorIndex and varBinds[int(errorIndex) - 1][0] or '?'))
        else:
            for varBind in varBinds:
                print(varBind)
                return varBind


    def snmp_walk(self,oid:str)->list:
        '''
        *oid: Start OID
        '''
        walk_info:list = []
        self.logger = logger('snmpwalk','iin')
        self.logger.log_info('Start SNMP Walk...')
        self.logger.log_info('IP address: %s:%s' %(self._entry.agent_ip,self._entry.agent_port))
        self.logger.log_info('Agent mode: %s' %(self._setup.ag_mode.name))
        self.logger.log_info('--------------------------------------------------------------------------')
        
        for (errorIndication, errorStatus, errorIndex, varBinds) in nextCmd(
            self._engine,
            self._setup.auth_data,
            UdpTransportTarget((self._entry.agent_ip,self._entry.agent_port)),
            ContextData(),
            ObjectType(ObjectIdentity(oid)),
            lexicographicMode=False
        ):
            if errorIndication:
                self.logger.log_info(errorIndication)
                break
            elif errorStatus:
                self.logger.log_info('%s at %s' % (errorStatus.prettyPrint(),
                                    errorIndex and varBinds[int(errorIndex) - 1][0] or '?'))
                break
            else:
                for varBind in varBinds: 
                    self.logger.log_info(varBind)
                    walk_info.append(varBind)
        
        
        self.logger.log_info('SNMP Walk finished.')
        return walk_info


    def trap_receiver(self)->deque:
        ''''''
        data_deque:deque[str] =[]

        self.logger = logger('Trap-Receiver','iin')
        self.logger.log_info('Agent is Receiving SNMP Trap from:')
        self.logger.log_info('IP address: %s:%s' %(self._entry.host_ip,self._entry.host_port))
        self.logger.log_info('Agent mode: %s' %(self._setup.ag_mode.name))
        self.logger.log_info('--------------------------------------------------------------------------')
        
        # Transport initialize
        if config.getTransport: 
            config.delTransport(self._engine,udp.domainName)

        config.addTransport(
            self._engine,udp.domainName,
            udp.UdpTransport().openServerMode((self._entry.host_ip,self._entry.host_port))
        )

        # v1/v2c and v3 setup
        if self._setup.ag_mode == Agent_Model.V1v2c:
            config.addV1System(
                self._engine,
                self._setup.community_index,self._setup.community_name
                )
        elif self._setup.ag_mode == Agent_Model.V3:
            config.addV3User(
                self._engine,self._setup.user_name,
                self._setup.public_Auth,self._setup.public_key,
                self._setup.private_Auth,self._setup.private_key,
                v2c.OctetString(hexValue=self._engine_id)
                )

        # Callback function for receiving notifications
        def cbFun(snmpEngine, stateReference, contextEngineId, contextName,varBinds, cbCtx):
            self.logger.log_info("Received new Trap message")
            # Resolve received data with mib
            varBinds = [rfc1902.ObjectType(rfc1902.ObjectIdentity(
                x[0]),x[1]).resolveWithMib(self.mibViewController) for x in varBinds]
            
            for varBind in varBinds:
                self.logger.log_info(varBind.prettyPrint())
                data_deque.append(str(varBind))

        # Register SNMP Application at the SNMP engine
        ntfrcv.NotificationReceiver(self._engine, cbFun)
        # Start transport dispatcher
        self._engine.transportDispatcher.jobStarted(1)  

        try:
            self.logger.log_info(
                'Trap Receiver started on port %s. Press Ctrl-c to quit.' % (self._entry.host_port))
            self._engine.transportDispatcher.runDispatcher()
        except:
            self.logger.log_info('Trap Receiver closed.')
            self._engine.transportDispatcher.closeDispatcher()
            return data_deque
            #raise


    def inform_generator(self,oid:str):
        iterator = sendNotification(
            self._engine,
            self._setup.auth_data,
            UdpTransportTarget((self._entry.host_ip,self._entry.host_port)),
            ContextData(),
            'inform',
            NotificationType(ObjectIdentity(oid))
        )

        errorIndication, errorStatus, errorIndex, varBinds = next(iterator)

        if errorIndication:
            print(errorIndication)
        elif errorStatus:
            print('%s at %s' % (errorStatus.prettyPrint(),
                                errorIndex and varBinds[int(errorIndex) - 1][0] or '?'))
        else:
            for varBind in varBinds:
                print(' = '.join([x.prettyPrint() for x in varBind]))


    def load_mib(self):
        self.mib_builder = builder.MibBuilder()
        compiler.addMibCompiler(self.mib_builder, 
            sources=['file:///usr/share/snmp/mibs','http://mibs.snmplabs.com/asn1/@mib@'])

        self.mibViewController = view.MibViewController(self.mib_builder)
        self.mib_builder.loadModules()

from snmp.snmp import *
from tests.init_cofig import *

test_oid:str = '1.3.6.1.2.1.1.5'
mib2_sys_oid:str ='.1.3.6.1.2.1.1'
engineid_oid:str ='1.3.6.1.6.3.10.2.1'

setups:classmethod = init_test('/home/tony/snmp-test/config/SNMP_v3')

def snmp_v3_walker(start_oid:str):
    snmp = SNMP(init_Entry(),snmp_setup=setups)
    get_info:list = snmp.snmp_walk(start_oid)

    #for info in get_info: print(info)

if __name__ == '__main__':
    snmp_v3_walker(mib2_sys_oid)
